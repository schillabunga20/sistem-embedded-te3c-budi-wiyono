1. Install Tarantool 2.6

root@osboxes:/home/osboxes# docker run -it tarantool/tarantool:2.6
Unable to find image 'tarantool/tarantool:2.6' locally
2.6: Pulling from tarantool/tarantool
31603596830f: Pull complete 
75dca67db1b9: Pull complete 
33d934a5b95d: Pull complete 
1851b00c7ea8: Pull complete 
913305879a41: Pull complete 
959b29dcd3c1: Pull complete 
8d2bd5e2c657: Pull complete 
4010315e71e6: Pull complete 
7d68f1746b1e: Pull complete 
6dc45f88d058: Pull complete 
1095af473e15: Pull complete 
5feedc453c65: Pull complete 
9bca7911c233: Pull complete 
a415ef777e65: Pull complete 
939351b42255: Pull complete 
ead58e4e83d4: Pull complete 
a1448f51caf2: Pull complete 
Digest: sha256:2ec7d362d014dce65952ea0251dbd4b96edef0cd7dd3aa97499ba32fbc4805ec
Status: Downloaded newer image for tarantool/tarantool:2.6
Creating configuration file: /etc/tarantool/config.yml
Config:
---
force_recovery: false
memtx_dir: /var/lib/tarantool
listen: 3301
pid_file: /var/run/tarantool/tarantool.pid
vinyl_dir: /var/lib/tarantool
wal_dir: /var/lib/tarantool
...

2021-02-07 08:41:08.138 [1] main/103/tarantool-entrypoint.lua C> Tarantool 2.6.2-0-g34d504d7d
2021-02-07 08:41:08.142 [1] main/103/tarantool-entrypoint.lua C> log level 5
2021-02-07 08:41:08.143 [1] main/103/tarantool-entrypoint.lua I> mapping 268435456 bytes for memtx tuple arena...
2021-02-07 08:41:08.144 [1] main/103/tarantool-entrypoint.lua I> Actual slab_alloc_factor calculated on the basis of desired slab_alloc_factor = 1.044274
2021-02-07 08:41:08.144 [1] main/103/tarantool-entrypoint.lua I> mapping 134217728 bytes for vinyl tuple arena...
2021-02-07 08:41:08.149 [1] main/103/tarantool-entrypoint.lua I> instance uuid 1faa79c4-96c4-431e-a309-a6ecf99b8159
2021-02-07 08:41:08.150 [1] iproto/101/main I> binary: bound to 0.0.0.0:3301
2021-02-07 08:41:08.155 [1] main/103/tarantool-entrypoint.lua I> initializing an empty data directory
2021-02-07 08:41:08.214 [1] main/103/tarantool-entrypoint.lua I> assigned id 1 to replica 1faa79c4-96c4-431e-a309-a6ecf99b8159
2021-02-07 08:41:08.214 [1] main/103/tarantool-entrypoint.lua I> cluster uuid 8850aa57-f3a5-4c5a-9b64-39306d80d206
2021-02-07 08:41:08.218 [1] snapshot/101/main I> saving snapshot `/var/lib/tarantool/00000000000000000000.snap.inprogress'
2021-02-07 08:41:08.235 [1] snapshot/101/main I> done
2021-02-07 08:41:08.237 [1] main/103/tarantool-entrypoint.lua I> ready to accept requests
2021-02-07 08:41:08.237 [1] main/103/tarantool-entrypoint.lua I> set 'log_level' configuration option to 5
2021-02-07 08:41:08.237 [1] main/105/checkpoint_daemon I> scheduled next checkpoint for Sun Feb  7 09:54:50 2021
2021-02-07 08:41:08.246 [1] main/103/tarantool-entrypoint.lua I> set 'listen' configuration option to "3301"
2021-02-07 08:41:08.246 [1] main/103/tarantool-entrypoint.lua I> set 'log_format' configuration option to "plain"
2021-02-07 08:41:08.251 [1] main/103/tarantool-entrypoint.lua I> Initializing database
2021-02-07 08:41:08.257 [1] main/103/tarantool-entrypoint.lua tarantool-entrypoint.lua:118 W> 
****************************************************
WARNING: 'guest' is chosen as primary user.
         Since it is not allowed to set a password for
         guest user, your instance will be accessible
         by anyone having direct access to the Tarantool
         port.
         If you wanted to create an authenticated user,
         specify "-e TARANTOOL_USER_NAME=username" and
         pick a user name other than "guest".
****************************************************
2021-02-07 08:41:08.258 [1] main/103/tarantool-entrypoint.lua I> Granting admin privileges to user 'guest'
2021-02-07 08:41:08.259 [1] main/116/console/unix/:/var/run/tarantool/tarantool.sock I> started
tarantool> 


2. Install Tarantool di Docker Engine

root@osboxes:/home/osboxes# curl -L https://tarantool.io/WrLJpp/realese/2.6/installer.sh | bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   184  100   184    0     0     24      0  0:00:07  0:00:07 --:--:--    44
  0     0    0     0    0     0      0      0 --:--:--  0:00:10 --:--:--     0bash: line 5: syntax error near unexpected token `newline'
bash: line 5: `<!DOCTYPE html>'
100 26132  100 26132    0     0   2308      0  0:00:11  0:00:11 --:--:-- 12251
curl: (23) Failed writing body (645 != 10393)
root@osboxes:/home/osboxes# curl -L https://tarantool.io/WrLJpp/release/2.6/installer.sh | bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   184  100   184    0     0    112      0  0:00:01  0:00:01 --:--:--   112
100 10304  100 10304    0     0   2117      0  0:00:04  0:00:04 --:--:--  6022
Detected operating system as ubuntu/xenial.
Setting up apt repository... 
Running apt-get update... done.
Checking for curl... 
Detected curl... done.
Checking for gpg... 
Detected gpg...
done.
Installing apt-transport-https... done.
Importing Tarantool gpg key... done.
The repository is setup! Tarantool can now be installed.
Running apt-get update... done.

Tarantool 2.6 is ready to be installed by Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  tarantool-common tarantool-dev
The following NEW packages will be installed:
  tarantool tarantool-common tarantool-dev
0 upgraded, 3 newly installed, 0 to remove and 416 not upgraded.
Need to get 7,295 kB of archives.
After this operation, 30.7 MB of additional disk space will be used.
Get:1 https://download.tarantool.org/WrLJpp/tarantool/release/2.6/ubuntu xenial/main amd64 tarantool-common all 2.6.2.0.g34d504d-1 [32.4 kB]
Get:2 https://download.tarantool.org/WrLJpp/tarantool/release/2.6/ubuntu xenial/main amd64 tarantool amd64 2.6.2.0.g34d504d-1 [7,223 kB]
Get:3 https://download.tarantool.org/WrLJpp/tarantool/release/2.6/ubuntu xenial/main amd64 tarantool-dev amd64 2.6.2.0.g34d504d-1 [39.3 kB]
Fetched 7,295 kB in 2min 27s (49.5 kB/s)
Selecting previously unselected package tarantool-common.
(Reading database ... 180857 files and directories currently installed.)
Preparing to unpack .../tarantool-common_2.6.2.0.g34d504d-1_all.deb ...
Unpacking tarantool-common (2.6.2.0.g34d504d-1) ...
Selecting previously unselected package tarantool.
Preparing to unpack .../tarantool_2.6.2.0.g34d504d-1_amd64.deb ...
Unpacking tarantool (2.6.2.0.g34d504d-1) ...
Selecting previously unselected package tarantool-dev.
Preparing to unpack .../tarantool-dev_2.6.2.0.g34d504d-1_amd64.deb ...
Unpacking tarantool-dev (2.6.2.0.g34d504d-1) ...
Processing triggers for man-db (2.7.5-1) ...
Processing triggers for systemd (229-4ubuntu21.16) ...
Processing triggers for ureadahead (0.100.0-19) ...
Setting up tarantool-common (2.6.2.0.g34d504d-1) ...
Setting up tarantool (2.6.2.0.g34d504d-1) ...
update-alternatives: using /usr/bin/tarantool to provide /usr/bin/lua (lua-interpreter) in auto mode
Setting up tarantool-dev (2.6.2.0.g34d504d-1) ...
Processing triggers for systemd (229-4ubuntu21.16) ...
Processing triggers for ureadahead (0.100.0-19) ...


3. Install Docker Compose

root@osboxes:/home/osboxes# sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   633  100   633    0     0    472      0  0:00:01  0:00:01 --:--:--   472
100 11.1M  100 11.1M    0     0   154k      0  0:01:13  0:01:13 --:--:--  157k
root@osboxes:/home/osboxes# sudo chmod +x /usr/local/bin/docker-composeroot@osboxes:/home/osboxes# docker-compose --versiondocker-compose version 1.23.1, build b02f1306

4. Tarantool container pada docker engine
root@osboxes:/home/osboxes# docker ps -a
CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS                      PORTS               NAMES
d54ae597bdd3        ubuntu                    "/bin/bash"              4 minutes ago       Exited (0) 4 minutes ago                        epic_ardinghelli
c326141f8021        tarantool/tarantool:2.6   "docker-entrypoint.s…"   About an hour ago   Exited (0) 30 minutes ago


